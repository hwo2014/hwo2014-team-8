#!/usr/bin/env ruby -w

HOST="senna.helloworldopen.com"
PORT=8091

track_name   = ARGV.shift
timo_count   = ARGV.shift.to_i
markus_count = ARGV.shift.to_i
slower_count = ARGV.shift.to_i


bot_sets = [
  {cmd: './join_race', name: 'SuperTimo', bot_count: timo_count},
  {cmd: './join_race', name: 'SuperMarkus', bot_count: markus_count},
  {cmd: './join_race_static', name: 'Slower', bot_count: slower_count}
]

puts bot_sets.to_s


class RaceRunner
  attr_reader :config, :track, :password

  def initialize(track, config)
    @track = track
    @password = (0...8).map { (65 + rand(26)).chr }.join
    @config = config
  end

  def run
    config.each do |bot_set|
      cmd_name = bot_set[:cmd]
      bot_name = bot_set[:name]
      count    = bot_set[:bot_count]

      (1..count).each do |i|
        join_race(cmd_name, "#{bot_name}#{i}")
      end
    end
  end

  private

  def join_race(cmd_name, bot_name)
    cmd = "#{cmd_name} #{HOST} #{PORT} #{bot_name} #{track} #{bot_count} #{password} > log/#{bot_name}.log &"
    puts "Executing: #{cmd}"
    system cmd
  
    sleep 1
  end

  def bot_count
    config.map {|c| c[:bot_count]}.reduce(0, :+)
  end

end

runner = RaceRunner.new(track_name, bot_sets)
runner.run
