
class StaticSpeedBot
  def initialize(server_host, server_port, bot_name, bot_key)
    @change_lane_sent_index = -1
    @turbo_available = false
    @turbo_enabled = false
    @turbo_factor = 1.0
    @last_throttle = 0.0

    @speed_test_phase = 3

    @static_throttle = 0.4 + Random.rand(0...0.1)
    puts "Advancing with static throttle: #{@static_throttle}"

    @static_friction_g_max = 0.0
    @acceleration = 0.0
    @velocity = 0.0
    @angle = 0.0
    @maximum_angle = 0.0
    @angular_velocity = 0.0
    @angular_acceleration = 0.0
    @maximum_acceleration = 0.0
    @minimum_acceleration = 0.0
    @minimum_acceleration_velocity = 0.0
    @is_at_curve = false
    @determine_acceleration_flag = true
    @maximum_angular_acceleration_coefficient = 0.0

    @server_host = server_host
    @server_port = server_port
    @bot_name = bot_name
    @bot_key = bot_key

    # Do not use visualization log in production:
    # puts "Writing visualization log to ../logs/visual.csv"
    # @visualization_log = File.open('../logs/visual.csv','w');
    # @visualization_log.puts "Tick;Angle;Velocity;Acceleration;Throttle;Radius;LaneDistance;ActualRadius;AngularVelocity;AngularAcceleration;StaticGMax;MaxVelCurve"

    @tcp = TCPSocket.open(@server_host, @server_port)
  end

  def join
    @tcp.puts join_message
    react_to_messages_from_server
  end

  def create_race(track, car_count, password)
    @tcp.puts create_message(track, car_count.to_i, password)
    react_to_messages_from_server
  end

  def join_race(track, car_count, password)
    puts "Joining a race with car count #{car_count}"
    @tcp.puts join_race_message(track, car_count.to_i, password)
    react_to_messages_from_server
  end

  private

  def react_to_messages_from_server
    while json = @tcp.gets
      message = JSON.parse(json)
      msgType = message['msgType']
      msgData = message['data']

      begin
        case msgType
          when 'carPositions'
            @tcp.puts throttle_message(@static_throttle)
          when 'join'
            puts "MSG: join"
          when 'gameInit'
            puts "MSG: gameInit"
          when 'gameStart'
            puts "MSG: gameStart"
            @tcp.puts ping_message
          when 'crash'
            puts "MSG: crash"
            if msgData['name'] == @bot_name then
              @last_throttle = 0
              @static_throttle = @static_throttle * 0.90
              puts " - adjusting static throttle: #{@static_throttle}"
            end
          when 'gameEnd'
            @tcp.puts ping_message
          when 'lapFinished'
            @tcp.puts ping_message
          when 'turboAvailable'
            puts "MSG: turboAvailable"
            @turbo_available = true
            @turbo_factor = msgData['turboFactor']
            @turbo_duration_ticks = msgData['turboDurationTicks']
            @turbo_duration_ms = msgData['turboDurationMilliseconds']
          when 'turboStart'
            puts "MSG: turboStart"
            @turbo_enabled = true
            puts message.to_s
          when 'turboEnd'
            puts "MSG: turboEnd"
            @turbo_enabled = false
            puts message.to_s
          when 'finish'
            puts "MSG: finish"
            #puts message.to_s

            puts "Detected variables:"
            puts " - maximum_angular_acceleration_coefficient: #{@maximum_angular_acceleration_coefficient}"
            puts " - maximum_angle: #{@maximum_angle}"
            puts " - static_friction_g_max: #{@static_friction_g_max}"
          when 'spawn'
            puts "MSG: spawn"
          when 'tournamentEnd'
            puts "MSG: tournamentEnd"
            #puts message.to_s
          when 'createRace'
            puts "MSG: createRace"
            puts message.to_s
            # @tcp.puts join_race_message(msgData['trackName'], msgData['carCount'], msgData['password'])
          when 'joinRace'
            puts "MSG: joinRace"
            puts message.to_s
          when 'yourCar'
            puts "MSG: yourCar"
            #puts message.to_s         
          when 'dnf' 
            puts "MSG: dnf"
            puts message.to_s         
          when 'error'
            puts "ERROR: #{msgData}"
            @tcp.puts ping_message
          else
            puts "UNHANDLED MSG: #{msgType}"
            @tcp.puts ping_message
        end
      rescue Exception => e
        puts "Caught exception: #{e}"
        @tcp.puts ping_message
      end
    end
  end

  def join_message
    make_msg("join", {:name => @bot_name, :key => @bot_key})
  end

  def create_message(track_name, car_count, password)
    make_msg("createRace", {:botId => {:name => @bot_name, :key => @bot_key}, :trackName => track_name, :carCount => car_count, :password => password})
  end

  def join_race_message_data(data)
    make_msg("joinRace", data)
  end

  def join_race_message(track_name, car_count, password)
    make_msg("joinRace", {:botId => {:name => @bot_name, :key => @bot_key}, :trackName => track_name, :carCount => car_count, :password => password})
  end


  def throttle_message(throttle)
    make_msg("throttle", throttle)
  end

  def switch_lane_message(lane)
    make_msg("switchLane", lane)
  end

  def turbo_message(lane)
    make_msg("turbo", lane)
  end

  def ping_message
    make_msg("ping", {})
  end

  def make_msg(msgType, data)
    JSON.generate({:msgType => msgType, :data => data})
  end

  # Helper methods, probably move somewhere

  # Maximum acceleration is the first velocity increase from 0 velocity.
  def estimate_maximum_acceleration_force(velocity)
    acceleration = @maximum_acceleration * (@turbo_enabled ? @turbo_factor : 1.0)
    acceleration - (velocity - acceleration) / 49.0
  end

  def estimate_maximum_deceleration_force(velocity)
    (velocity - @minimum_acceleration_velocity) / -49.0 + @minimum_acceleration
  end

  def estimate_acceleration(velocity, throttle)
    estimate_maximum_acceleration_force(velocity) * throttle + estimate_maximum_deceleration_force(velocity) * (1.0 - throttle)
  end

  def maximum_velocity_at_curve(curve, position)
    actual_radius = curve.radius + (curve.to_the_right? ? -@position.start_lane.distance_from_center : @position.start_lane.distance_from_center)
    Math.sqrt(@static_friction_g_max * actual_radius)
  end

  def braking_distance(from_velocity, to_velocity, throttle)
    # warning, this may never return if throttle is too high!
    distance = 0.0
    velocity = from_velocity
    while velocity > to_velocity
      velocity += estimate_acceleration(velocity, throttle)
      distance += velocity
    end
    distance
  end

end