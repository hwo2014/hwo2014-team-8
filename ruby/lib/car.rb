class Car
  attr_accessor :track, :name, :color, :position_history, :length

  def initialize(name, color, length)
    @name = name
    @color = color
    @position_history = []
    @length = length
  end

  def self.parse_from_json(json)
    puts json
    Car.new(json['id']['name'], json['id']['color'], json['dimensions']['length'])
  end

  def start_lane
    return nil if @start_lane.nil?
    track.find_lane_by_index(@start_lane)
  end

  def end_lane
    track.find_lane_by_index(@end_lane)
  end

  def add_position(position)
    position_history << position
  end

  def current_position
    position_history[-1]
  end

  # -1 means the last position in history
  def velocity(position_history_id=-1)
    return 0.0 if @position_history.size < 2

    position_history[position_history_id] - position_history[position_history_id - 1]
  end

  def acceleration
    velocity(-1) - velocity(-2)
  end

  def to_s
    "#{name} (#{color})"
  end

  def finished?
    current_position.lap >= track.race_session.laps
  end

  def opponent_in_the_way?
    debug = false
    track.blocked_lanes(debug).include?(current_position.lane_end_index)
  end

  def last_lap?
    current_position.lap + 1 >= track.race_session.laps 
  end

end
