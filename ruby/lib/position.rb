class Position
  include Comparable

  attr_reader :track, :piece_index, :lane_start_index, :lane_end_index, :lane_index, :in_piece_distance, :angle
  attr_accessor :lap 

  def initialize(track, lap, piece_index, in_piece_distance, lane_start_index, lane_end_index, angle)
    @track = track
    @lap = lap
    @piece_index = piece_index
    @in_piece_distance = in_piece_distance
    @lane_start_index = lane_start_index
    @lane_end_index = lane_end_index
    @angle = angle

    if @lap < 0
      puts "Forcing lap #{@lap} into 0"
      @lap = 0
    end
  end

  # {"pieceIndex"=>0, "inPieceDistance"=>0.0, "lane"=>{"startLaneIndex"=>0, "endLaneIndex"=>0}, "lap"=>0}
  def self.parse_from_json(track, json)
    piece_position_json = json['piecePosition']
    Position.new(track, 
      json['piecePosition']['lap'], json['piecePosition']['pieceIndex'], json['piecePosition']['inPieceDistance'], 
      json['piecePosition']['lane']['startLaneIndex'], json['piecePosition']['lane']['endLaneIndex'], 
      json['angle'])
  end

  def piece
    track.find_piece_by_lap_and_index(@lap, @piece_index)
  end

  def start_lane
    track.find_lane_by_index(@lane_start_index)
  end

  def end_lane
    track.find_lane_by_index(@lane_end_index)
  end

  # This (perhaps wrongly) assumes that the leftmost lane has index 0, and rightmost the amount of lanes
  def at_leftmost_lane?
    @lane_start_index == 0
  end

  def at_rightmost_lane?
    @lane_start_index == @track.lanes.size - 1
  end

  # TODO check lanes correctly
  def -(position)
    distance_between(position, self)
  end

  # Returns distance from pos1 to pos2 through current lane 
  def distance_between(pos1, pos2)
    return 0.0 if pos1.nil? || pos2.nil?

    distance = 0
    if pos1.piece_index == pos2.piece_index
      distance = pos2.in_piece_distance - pos1.in_piece_distance
    else
      if pos1 < pos2
        # starting piece
        
        first_piece_distance = pos1.piece.length(pos1.end_lane.distance_from_center) - pos1.in_piece_distance
        
        full_pieces_distance = 0
        full_pieces = @track.pieces_in_between_exclusive(pos1.piece, pos2.piece)
        full_pieces.each do |piece|
          #puts "FULL PIECE"
          full_pieces_distance += piece.length(start_lane.distance_from_center)
        end
        # last piece
        last_piece_distance = pos2.in_piece_distance

        #puts "distance_from_center: #{start_lane.distance_from_center}"
        #puts "first: #{first_piece_distance}, full: #{full_pieces_distance}, last: #{last_piece_distance}"

        distance = first_piece_distance + full_pieces_distance + last_piece_distance
      else
        distance = -distance_between(pos2, pos1)
      end
    end
    distance
  end

  def to_s
    '%u/%u/%.2f lane: %u->%u' % [@lap, @piece_index, @in_piece_distance, @lane_start_index, @lane_end_index]
  end

  def <=>(another)
    [piece, in_piece_distance] <=> [another.piece, another.in_piece_distance]
  end

  def in_piece_distance_left
    piece.length_on_lane_index(lane_end_index) - in_piece_distance # sinne päin
  end

end
