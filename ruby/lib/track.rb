class Track
  attr_accessor :pieces, :cars, :lanes, :pieces_per_lap, :race_session, :own_car

  def initialize
    @pieces = []
    @lanes  = []
    @cars   = []
  end

  def self.parse_from_json(json)
    track = Track.new
    data = json['data']
    race = data['race']
    track_json = race['track']
    #puts
    #puts "JSON: " + json.to_s
    #puts

    # In CI (and actual race) raceSession is as follows in the qualification round:
    # "raceSession"=>{"durationMs"=>20000}

    track.race_session = RaceSession::parse_from_json(race['raceSession'])

    # Let's add pieces for each individual lap so they can be traversed more easily
    # Qualification phase has 999 laps
    laps = track.race_session.laps

    piece_id = 0
    0.upto(laps - 1) do |lap|
      piece_index = 0
      track_json['pieces'].each do |piece_json|
        piece = Piece::parse_from_json(track, lap, piece_index, piece_id, piece_json)
        track.add_piece(piece)
        piece_index += 1
        piece_id += 1
      end
    end

    track.pieces_per_lap = track_json['pieces'].size

    race['cars'].each do |car_json|
      car = Car::parse_from_json(car_json)
      track.add_car(car) # TODO race
    end

    track_json['lanes'].each do |lane_json|
      lane = Lane::parse_from_json(lane_json)
      track.add_lane(lane)
    end

    track
  end

  def add_lane(lane)
    @lanes << lane
    lane.track = self
  end

  def add_piece(piece)
    @pieces << piece
  end

  def add_car(car)
    @cars << car
    car.track = self
  end

  def find_lane_by_index(index)
    @lanes[index]
  end

  def find_car_by_name(name)
    @cars.each do |car|
      return car if car.name == name
    end
    raise "Cannot find car: #{name}"
  end

  # TODO extract this into PiecesCollection etc.

  def find_piece_by_lap_and_index(lap, piece_index)
    raise "Invalid piece index: " + piece_index if piece_index < 0 || piece_index > @pieces.size 
    lap = 0 if lap < 0
    @pieces[@pieces_per_lap * lap + piece_index]
  end

  def find_piece_by_piece_id(piece_id)
    return nil if piece_id < 0 || piece_id >= @pieces.size
    @pieces[piece_id]
  end

  def pieces_in_between_exclusive(piece1, piece2)
    @pieces[(piece1.piece_id + 1)..(piece2.piece_id - 1)]
  end

  def pieces_in_between_inclusive(piece1, piece2)
    @pieces[piece1.piece_id, piece2.piece_id]
  end

  def switch_pieces_after_position(position)
    nextpiece = position.piece.next
    pieces = nextpiece.nil? ? [] : @pieces[nextpiece.piece_id, @pieces.size - 1]

    pieces.select { |piece| piece.switch? }
  end

  # return the next curve piece from this position (do not return the same piece ever)
  def find_next_curve(position)
    piece = position.piece.next
    while !piece.nil? && !piece.curve?
      piece = piece.next
    end
    return piece
  end

  # return the distance to the beginning of the defined piece
  def distance_to_piece(position, piece)
    distance = position.in_piece_distance_left
    pieces_in_between_exclusive(position.piece, piece).each { |piece| distance = distance + piece.length(position.end_lane.distance_from_center) }
    distance
  end

  def longest_straight_length
    length = max_length = 0.0
    @pieces.each do |piece|
      if piece.straight?
        length += piece.length
      else
        max_length = length if max_length < length
        length = 0.0
      end
    end
    max_length
  end

  def width
    @width ||= (lanes.last.distance_from_center - lanes.first.distance_from_center) 
  end

  def lap_length
    distance_from_center = 0.0
    @lap_length ||= pieces.take(pieces_per_lap).map {|piece| piece.length(distance_from_center)}.reduce(0, :+)
  end

  def blocked_lanes(debug=true)
    puts "Calculating blocked lanes" if debug
    res = []
    car_length = own_car.length
    too_close_limit = car_length * 1.5 # TODO adjust dynamically based on velocity difference or distance between the switches
    puts "- too_close_limit #{too_close_limit}, lap_length: #{lap_length}" if debug

    cars.each do |car|
      if car.finished?
        puts "- car #{car.to_s} has finished" if debug
        next
      end

      next if car == own_car
      other_car_pos = car.current_position.clone
      diff = other_car_pos - own_car.current_position

      while diff > lap_length * 0.5 && other_car_pos.lap > 0
        puts "  * car #{car.to_s} is leading by more than half lap (diff: #{diff})" if debug
        other_car_pos.lap -= 1
        diff = other_car_pos - own_car.current_position
      end

      while diff < -lap_length * 0.5 && other_car_pos.lap < race_session.laps - 1
        puts "  * car #{car.to_s} is behind by more than half lap (diff: #{diff})" if debug
        other_car_pos.lap += 1
        diff = other_car_pos - own_car.current_position
        too_close_limit = car_length * 2.5
      end

      puts "- car #{car.to_s}: #{diff}" if debug
      next if diff < 0.0 # we are leading
      next if diff > too_close_limit # far ahead
      puts "- car #{car.to_s} blocks lane #{car.current_position.lane_end_index} (diff: #{diff}"
      res << car.current_position.lane_end_index
    end
    res.uniq
  end

end