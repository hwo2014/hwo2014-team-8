
class Lane
  attr_accessor :distance_from_center, :index, :track

  def initialize(distance_from_center, index)
    @distance_from_center = distance_from_center
    @index =  index
  end

  def self.parse_from_json(json)
    Lane.new(json['distanceFromCenter'], json['index'])
  end

  def possible_lane_indexes_after_switch
    res = []
    res << index
    res << index - 1 unless leftmost?
    res << index + 1 unless rightmost?
    res
  end

  def leftmost?
    @index == 0
  end

  def rightmost?
    @index == @track.lanes.size - 1
  end
end
