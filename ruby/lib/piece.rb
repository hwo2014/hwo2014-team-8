# coding: utf-8

class Piece
  include Comparable

  attr_reader :track, :piece_index, :lap, :length, :switch, :radius, :angle, :piece_id

  def initialize(track, lap, piece_index, piece_id, length: 0.0, switch: false, radius:0.0, angle: 0.0)
    @track = track
    @lap = lap
    @piece_index = piece_index
    @piece_id = piece_id
    @length = length
    @switch = switch
    @radius = radius
    @angle = angle
  end

  def self.parse_from_json(track, lap, piece_index, piece_id, json)
    angle = json['angle'] || 0.0
    radius = json['radius'] || 0.0
    switch = json['switch'] || false
    length = json['length']

    Piece.new(track, lap, piece_index, piece_id, length: length, switch: switch, angle: angle, radius: radius)
  end

  def self.create_straight(track, lap, piece_index, length)
    Piece.new(track, lap, piece_index, length: length)
  end

  def self.create_curve(track, lap, piece_index, radius, angle)
    Piece.new(track, lap, piece_index, radius: radius, angle: angle)
  end

  def <=>(another)
    return 0 if @lap.nil? || another.nil?
    [@lap, @piece_index] <=> [another.lap, another.piece_index]
  end

  def straight?
    angle == 0.0
  end

  def curve?
    angle != 0.0
  end

  def to_the_right?
    raise "not a curve!" unless curve?
    !! (angle > 0.0)
  end

  def to_the_left?
    raise "not a curve" unless curve?
    angle < 0.0
  end

  def switch?
    switch
  end

  def length(distance_from_center=0.0)
    # TODO handle switching
    straight? ? @length : 2 * Math::PI * lane_radius(distance_from_center) * angle.abs / 360.0
  end

  def length_on_lane_index(lane_index)
    lane = track.find_lane_by_index(lane_index)
    length(lane.distance_from_center)
  end

  def length_on_lane(from_lane, to_lane=nil)
    if switch? && from_lane != to_lane
      raise "missing to_lane" if to_lane.nil?

      # TODO curve switch is a little bit longer?
      distance_from_center = (from_lane.distance_from_center + to_lane.distance_from_center) / 2.0
      distance_without_switch = length(distance_from_center)
      Math.sqrt(distance_without_switch ** 2 + track.width ** 2)
    else
      distance_from_center = from_lane.distance_from_center
      length(distance_from_center)
    end
  end

  def lane_radius(distance_from_center)
    if to_the_left?
      @radius + distance_from_center
    elsif to_the_right?
      @radius - distance_from_center
    else
      raise "not a curve!"
    end
  end

  def to_s
    st = "#{@lap}/#{piece_index} "
    if straight?
      st += "straight,%0.2f,#{switch? ? 'SW' : '-'}" % [@length] 
    else 
      direction = to_the_right? ? "->" : "<-"
      st += "curve,%0.2f,%0.2f,#{direction},#{switch? ? 'SW' : '-'}" % [@radius, @angle]
    end
    st
  end

  def next
    track.find_piece_by_piece_id(@piece_id + 1)
  end

  def previous
    track.find_piece_by_piece_id(@piece_id - 1)
  end

end