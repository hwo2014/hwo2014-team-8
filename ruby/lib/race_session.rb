class RaceSession
  attr_reader :laps, :max_lap_time_ms, :quickrace, :duration_ms

  def initialize(laps, max_lap_time_ms, quickrace, duration_ms)
    @laps = laps
    @max_lap_time_ms = max_lap_time_ms
    @quickrace = quickrace
    @duration_ms = duration_ms
  end

  def self.parse_from_json(json)
    laps = json['laps']
    max_lap_time_ms = json['maxLapTimeMs']
    quickrace = json['quickrace']
    duration_ms = json['durationMs']
    if laps.nil? # Qualification
      laps = 999
      max_lap_time_ms = 99999.0
      quickrace = false
    else # Race
      duration_ms = 99999.0
    end
    RaceSession.new(laps, max_lap_time_ms, quickrace, duration_ms)
  end

  def quickrace?
    quickrace
  end
end
