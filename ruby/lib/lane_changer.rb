class LaneChanger
  attr_reader :track, :graph

  LIMIT_SWITCH_COUNT = 20

  def initialize(track)
    @track = track
    @graph = Graph.new
  end

  def all_lanes
    @track.lanes
  end

  def all_pieces
    @track.pieces
  end

  def calculate_optimal_route(current_position)
    blocked_lanes = track.blocked_lanes
    puts "Blocked lanes: #{blocked_lanes.to_s}"

    switch_pieces = track.switch_pieces_after_position(current_position)
    unless switch_pieces
      "No switch pieces left, returning"
    end

    puts "- Found #{switch_pieces.size} upcoming switch pieces"

    if switch_pieces.size > LIMIT_SWITCH_COUNT
      puts "   * limiting to #{LIMIT_SWITCH_COUNT}"
      switch_pieces = switch_pieces.take(LIMIT_SWITCH_COUNT)
      #target = [switch_pieces.last.piece_id, (track.lanes.size / 2).to_i]
      #puts "   * target: #{target.to_s}"

      piece = switch_pieces.last.next || switch_pieces.last
      all_lanes.each do |lane|
        graph.add_edge([piece.piece_id, lane.index], "FINISH", 0.0) # XXX super-ugly :)
      end

      limitted = true
    else 
      limitted = false
    end
    target ||= "FINISH"

    # 1) Add switch pieces to graph
    switch_pieces.each do |piece|
      all_lanes.each do |from_lane|
        from_lane_index = from_lane.index
        from_lane.possible_lane_indexes_after_switch.each do |to_lane_index|
          to_lane = track.find_lane_by_index(to_lane_index)
          distance = piece.length_on_lane(from_lane, to_lane)
          if piece.next
            puts "- switch from piece #{piece.piece_id} to #{piece.next.nil? ? "nil" : piece.next.piece_id } from lane #{from_lane_index} to #{to_lane_index} => #{distance}"
            graph.add_edge [piece.piece_id, from_lane_index], [piece.next.piece_id, to_lane_index], distance
          else
            puts "- switch from piece #{piece.piece_id} to FINISH from lane #{from_lane_index} to #{to_lane_index} => #{distance}"
            graph.add_edge [piece.piece_id, from_lane_index], "FINISH", distance
          end
        end
      end   
    end

    # 2) Add normal (non-switch) pieces in between switch pieces into graph

    puts "- adding normal edges"
    switch_pairs = switch_pieces.each_cons(2)
    switch_pairs.each do |switch1, switch2|
      all_lanes.each do |lane|
        pieces = @track.pieces_in_between_exclusive(switch1, switch2)
        if pieces
          distance = pieces.map { |piece| piece.length_on_lane(lane) }.reduce(0, :+)
          puts "- normal from piece #{pieces.first.piece_id} to #{switch2.piece_id} on lane #{lane.index} => #{distance}"
          if blocked_lanes.include?(lane.index)
            puts "  * BLOCKED"
            distance *= 2.0
          end
          graph.add_edge([pieces.first.piece_id, lane.index], [switch2.piece_id, lane.index], distance)
        else
          puts "- no normal pieces after piece #{from_piece.piece_id}"
        end
      end
    end

    # 3) Add last pieces from the piece after last switch to the finish
    unless limitted
      last_switch_piece = switch_pieces.last
      piece = last_switch_piece.next
      if piece
        all_lanes.each do |lane|
          distance = piece ? distance_from_piece_to_end_of_track(piece, lane) : 0.0 # switch can be last block
          puts "- last part from piece #{piece.piece_id} to finish on lane #{lane.index} => #{'%.2f' % distance}"
          graph.add_edge([piece.piece_id, lane.index], "FINISH", distance) # XXX super-ugly :)
        end
      else
        puts "- last switch already finished, nothing needs to be added to graph"
      end
    end

    puts "Finding shortest path from switch piece #{switch_pieces.first.piece_id} / lane #{current_position.end_lane.index}"

    path = graph.shortest_path([switch_pieces.first.piece_id, current_position.end_lane.index], target)
    puts "OPTIMAL PATH: "
    puts path.to_s

    if path.nil? || path.size < 2
      puts "- CANNOT FIND PATH! : #{path.to_s}"
      return nil
    end
    # TODO would be better to add new virtual piece if last piece is switch
    if path[1] == "FINISH"
      puts "- Last piece -> don't change lane"
      return nil
    end
    first_lane  = path[0][1]
    second_lane = path[1][1]
    if first_lane < second_lane
      return "Right"
    elsif first_lane > second_lane
      return "Left"
    else 
      return nil
    end
  end

  def distance_from_piece_to_end_of_track(piece, lane)
    pieces = track.pieces_in_between_inclusive(piece, track.pieces.last)
    pieces.map { |piece| piece.length_on_lane(lane) }.reduce(0.0, :+)
  end

  # including first piece (piece1), excluding last piece (piece2)
  def distance_through_lane(piece1, piece2, start_lane, end_lane)
    # TODO calculate distnace in switch correctly (for the first piece)
    distance = 0.0
    piece = piece1
    while piece && piece != piece2
      distance += piece.length(lane.distance_from_center)
      piece = piece.next
    end
    distance
  end
end


class Graph
  def initialize
    @g = {}  # the graph // {node => { edge1 => weight, edge2 => weight}, node2 => ...
    @nodes = Array.new     
    @INFINITY = 1 << 64    
  end
    
  def add_edge(s,t,w)     # s= source, t= target, w= weight
    if (not @g.has_key?(s))  
      @g[s] = {t=>w}     
    else
      @g[s][t] = w         
    end
    
    # Begin code for non directed graph (inserts the other edge too)
    
    if (not @g.has_key?(t))
      @g[t] = {s=>w}
    else
      @g[t][s] = w
    end

    # End code for non directed graph (ie. deleteme if you want it directed)

    if (not @nodes.include?(s)) 
      @nodes << s
    end
    if (not @nodes.include?(t))
      @nodes << t
    end 
  end
  
  # based of wikipedia's pseudocode: http://en.wikipedia.org/wiki/Dijkstra's_algorithm
  
  def dijkstra(s)
    @d = {}
    @prev = {}

    @nodes.each do |i|
      @d[i] = @INFINITY
      @prev[i] = -1
    end 

    @d[s] = 0
    q = @nodes.compact
    while (q.size > 0)
      u = nil;
      q.each do |min|
        if (not u) or (@d[min] and @d[min] < @d[u])
          u = min
        end
      end
      if (@d[u] == @INFINITY)
        break
      end
      q = q - [u]
      @g[u].keys.each do |v|
        alt = @d[u] + @g[u][v]
        if (alt < @d[v])
          @d[v] = alt
          @prev[v]  = u
        end
      end
    end
  end
  
  # To print the full shortest route to a node
  
  def print_path(dest)
    if @prev[dest] != -1
      print_path @prev[dest]
    end
    print ">#{dest}"
  end

  def get_path(dest)
    res = []
    if @prev[dest] != -1
      res = get_path @prev[dest]
    end
    res << dest
    res
  end
  
  # Gets all shortests paths using dijkstra
  
  def shortest_paths(s)
    @source = s
    dijkstra s
    puts "Source: #{@source}"
    @nodes.each do |dest|
      puts "\nTarget: #{dest}"
      print_path dest
      if @d[dest] != @INFINITY
        puts "\nDistance: #{@d[dest]}"
      else
        puts "\nNO PATH"
      end
    end
  end

  def shortest_path(s, to)
    @source = s
    dijkstra s
    @nodes.each do |dest|
      next if dest != "FINISH"
      puts "\nTarget: #{dest}"
      print_path dest      
      if @d[dest] != @INFINITY
        puts "\nDistance: #{@d[dest]}"
        return get_path dest
      else
        puts "\nNO PATH"
        return nil
      end
    end    
  end
end

#gr.shortest_paths("a")
