
class NorsuBot
  CRASH_ON_EXCEPTIONS = false

  def initialize(server_host, server_port, bot_name, bot_key)
    @turbo_available = false
    @turbo_enabled = false
    @turbo_factor = 1.0
    @last_throttle = 0.0
    @am_i_crashed = false

    @speed_test_phase = 3

    @static_friction_g_max = 0.0
    @acceleration = 0.0
    @velocity = 0.0
    @angle = 0.0
    @maximum_angle = 0.0
    @angular_velocity = 0.0
    @angular_acceleration = 0.0
    @maximum_acceleration = 0.0
    @minimum_acceleration = 0.0
    @minimum_acceleration_velocity = 0.0
    @is_at_curve = false
    @determine_acceleration_flag = true
    @maximum_angular_acceleration_coefficient = 0.0

    @magic_curve_velocity_coefficient = 1.1

    @maximum_slip_angle = 60.0

    @server_host = server_host
    @server_port = server_port
    @bot_name = bot_name
    @bot_key = bot_key

    # Do not use visualization log in production:
    # puts "Writing visualization log to ../logs/visual.csv"
    # @visualization_log = File.open('../logs/visual.csv','w');
    # @visualization_log.puts "Tick;Angle;Velocity;Acceleration;Throttle;Radius;LaneDistance;ActualRadius;AngularVelocity;AngularAcceleration;StaticGMax;MaxVelCurve;EstimatedAcceleration"

    @tcp = TCPSocket.open(@server_host, @server_port)
  end

  def join
    @tcp.puts join_message
    react_to_messages_from_server
  end

  def create_race(track, car_count, password)
    @tcp.puts create_message(track, car_count.to_i, password)
    react_to_messages_from_server
  end

  def join_race(track, car_count, password)
    puts "Joining a race with car count #{car_count}"
    @tcp.puts join_race_message(track, car_count.to_i, password)
    react_to_messages_from_server
  end

  private

  def react_to_messages_from_server
    while json = @tcp.gets
      message = JSON.parse(json)
      msgType = message['msgType']
      msgData = message['data']

      begin
        case msgType
          when 'carPositions'
            if !message['gameTick'].nil?
              parse_pos(message)
              make_move
            else
              @tcp.puts ping_message
            end
          when 'join'
            puts "MSG: join"
            #puts message.to_s
          when 'gameInit'
            puts "MSG: gameInit"
            #puts message.to_s
            parse_init(message)
          when 'gameStart'
            puts "MSG: gameStart"
            puts message.to_s
            @tcp.puts ping_message

            @turbo_available = false
            @turbo_enabled = false
            @am_i_crashed = false
          when 'crash'
            puts "MSG: crash"
            if msgData['name'] == @bot_name 
              @last_throttle = 0
              @am_i_crashed = true

              if @maximum_angle < @angle then
                @maximum_angle = @angle
              end

              puts " - You crashed!"
              puts "   * maximum_angular_acceleration_coefficient: #{@maximum_angular_acceleration_coefficient}"
              puts "   * maximum_angle: #{@maximum_angle}"
              puts "   * static_friction_g_max: #{@static_friction_g_max}"

              @maximum_slip_angle = @maximum_angle

              puts "   * Storing maximum slip angle: #{@maximum_slip_angle}"

              if @turbo_available 
                puts " - You had a turbo and you lost it."
                @turbo_available = false
              end

              if @position.piece.curve? && !@turbo_enabled 
                puts " - Crashed in curve, reseting magic coefficient"
                if @magic_curve_velocity_coefficient == 1.0 
                  @static_friction_g_max = @static_friction_g_max * 0.90
                else
                  @magic_curve_velocity_coefficient = 1.0
                end
              end
            else
              puts " - Somebody else crashed: #{message.to_s}"
            end
          when 'gameEnd'
            #puts "MSG: gameEnd"
            #puts message.to_s
            @tcp.puts ping_message
          when 'lapFinished'
            #puts "MSG: lapFinished"
            #puts message.to_s
            @tcp.puts ping_message
          when 'turboAvailable'
            puts "MSG: turboAvailable"
            puts message.to_s
            if @am_i_crashed then
              puts " - I've crashed so no turbo for me"
            else
              @turbo_available = true
              @turbo_factor = msgData['turboFactor']
              @turbo_duration_ticks = msgData['turboDurationTicks']
              @turbo_duration_ms = msgData['turboDurationMilliseconds']
              puts " - You received a turbo!"
            end
          when 'turboStart'
            puts "MSG: turboStart"
            if msgData['name'] == @bot_name then
              puts " - My turbo started"
              @turbo_enabled = true
            else
              puts message.to_s
            end
          when 'turboEnd'
            puts "MSG: turboEnd"
            puts message.to_s
            if msgData['name'] == @bot_name then
              puts " - My turbo ended"
              @turbo_enabled = false
            else
              puts message.to_s
            end
          when 'finish'
            puts "MSG: finish"
            puts "Detected variables:"
            puts " - maximum_angular_acceleration_coefficient: #{@maximum_angular_acceleration_coefficient}"
            puts " - maximum_angle: #{@maximum_angle}"
            puts " - static_friction_g_max: #{@static_friction_g_max}"
          when 'spawn'
            puts "MSG: spawn"
            if msgData['name'] == @bot_name then
              @am_i_crashed = false
              puts " - I got back on track!"
            else
              puts message.to_s
            end
          when 'tournamentEnd'
            puts "MSG: tournamentEnd"
            #puts message.to_s
          when 'createRace'
            puts "MSG: createRace"
            puts message.to_s
          when 'joinRace'
            puts "MSG: joinRace"
            puts message.to_s
          when 'yourCar'
            puts "MSG: yourCar"
            #puts message.to_s         
          when 'dnf' 
            puts "MSG: dnf"
            puts message.to_s         
          when 'error'
            puts "ERROR: #{msgData}"
            @tcp.puts ping_message
          else
            puts "UNHANDLED MSG: #{msgType}"
            @tcp.puts ping_message
        end
      rescue StandardError => e
        puts "*** Caught exception: #{e} *** "
        puts e.backtrace
        puts "Failed on JSON:"
        puts message.to_s
        @tcp.puts ping_message
        raise e if CRASH_ON_EXCEPTIONS
      end
    end
  end

  def parse_init(msg)
    @track = Track::parse_from_json(msg)

    puts "Init JSON"
    puts msg.to_s
    puts

    @longest_straight_length = @track.longest_straight_length

    puts "Longest straight in track: #{@longest_straight_length}"
  end

  def parse_pos(msg)
    # {"msgType"=>"carPositions", "data"=>[{"id"=>{"name"=>"Kood.io", "color"=>"red"}, "angle"=>0.0, "piecePosition"=>{"pieceIndex"=>0, "inPieceDistance"=>2.6907556454464, "lane"=>{"startLaneIndex"=>0, "endLaneIndex"=>0}, "lap"=>0}}], "gameId"=>"d8bca900-5ca1-423b-b906-cc427c6c29d4", "gameTick"=>8

    @tick = msg['gameTick']

    data = msg['data']

    own_car = nil
    data.each do |car_json|
      bot_name = car_json['id']['name']
      car = @track.find_car_by_name(bot_name)
      car.add_position Position::parse_from_json(@track, car_json)
    end

    own_car   = @track.find_car_by_name(@bot_name)
    return if own_car.finished?
    @track.own_car = own_car
    @position = own_car.current_position

    @last_angle = @angle
    @angle    = @position.angle
    @lap      = @position.lap
    @velocity     = own_car.velocity
    @acceleration = own_car.acceleration

    @last_angular_velocity = @angular_velocity
    @angular_velocity = @angle - @last_angle

    @last_angular_acceleration = @angular_acceleration
    @angular_acceleration = @angular_velocity - @last_angular_velocity
    delta_angular_acceleration = @angular_acceleration - @last_angular_acceleration

    @opponent_in_the_way = own_car.opponent_in_the_way?

    if @maximum_angle < @angle then
      @maximum_angle = @angle
    end

    if @speed_test_phase == 3 # Full acceleration (throttle = 1.0)
      if @acceleration > @maximum_acceleration
        @maximum_acceleration = @acceleration
      end
      if @velocity > 0.0
        puts "Got maximum acceleration sample: #{@maximum_acceleration}"
        @speed_test_phase = 2
      end
    elsif @speed_test_phase == 2 # Full brake (throttle = 0.0)
      if @acceleration < @minimum_acceleration
        @minimum_acceleration = @acceleration
        @minimum_acceleration_velocity = @velocity
      end

      if @minimum_acceleration < 0
        puts "Got deceleration sample, ending phase 2. Maximum deceleration: #{@minimum_acceleration} @ #{@minimum_acceleration_velocity}"
        @speed_test_phase = 1
      end
    end

    estimated_acceleration = estimate_acceleration(@velocity, @last_throttle)
    piece = @position.piece

    @was_at_curve = @is_at_curve

    if piece.curve?
      @is_at_curve = true
      maximum_velocity = maximum_velocity_at_curve(piece, @position)
      actual_radius = piece.radius + (piece.to_the_right? ? -@position.start_lane.distance_from_center : @position.start_lane.distance_from_center)
      corrected_velocity_at_curve = @velocity - maximum_velocity
    else
      @is_at_curve = false
      @determine_acceleration_flag = true
      maximum_velocity = 0.0
      actual_radius = 0.0
    end

    if @was_at_curve && @is_at_curve && @determine_acceleration_flag
      if corrected_velocity_at_curve > 0.0 && @angle != 0.0 then
        angular_acceleration_coefficient = delta_angular_acceleration / corrected_velocity_at_curve

        if angular_acceleration_coefficient.abs > @maximum_angular_acceleration_coefficient
          @maximum_angular_acceleration_coefficient = angular_acceleration_coefficient.abs
          puts "Detected angular acceleration coefficient: #{@maximum_angular_acceleration_coefficient}"
        end
      end
      @determine_acceleration_flag = false
    end

    if @am_i_crashed
      puts "t: #{@tick}, car: [#{own_car.name}] - crashed, waiting for spawn..."
    else
      puts "t: #{@tick}, car: [#{own_car.name}], ta?:#{@turbo_available}, block?:#{@opponent_in_the_way}, ll?:#{own_car.last_lap?}, pc: [%s], pos: [%s], thr: %.2f, ang: %.2f, vel: %.5f, acc: %.5f (est %.5f, diff %.5f, coeff %.5f)" %
        [@position.piece.to_s, @position.to_s, @last_throttle, @angle, @velocity, @acceleration, estimated_acceleration, estimated_acceleration - @acceleration, estimate_acceleration_coefficient]
    end

    # @visualization_log.puts "#{@tick};%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s" % [
    #   @angle.to_s.gsub('.', ','),
    #   @velocity.to_s.gsub('.', ','),
    #   @acceleration.to_s.gsub('.', ','),
    #   @last_throttle.to_s.gsub('.', ','),
    #   piece.curve? ? piece.radius.to_s.gsub('.', ',') : '',
    #   @position.start_lane.nil? ? '' : @position.start_lane.distance_from_center,
    #   actual_radius.to_s.gsub('.', ','),
    #   @angular_velocity.to_s.gsub('.', ','),
    #   @angular_acceleration.to_s.gsub('.', ','),
    #   @static_friction_g_max.to_s.gsub('.', ','),
    #   maximum_velocity.to_s.gsub('.', ','),
    #   estimated_acceleration.to_s.gsub('.', ',')
    # ]

    if @is_at_curve && @was_at_curve && @angle == 0.0 && @speed_test_phase == 1
      # Don't check static friction coefficient on first tick, but second
      static_friction_g = @velocity * @velocity / actual_radius
      if static_friction_g > @static_friction_g_max
        @static_friction_g_max = static_friction_g
        puts " - Store static friction coefficient * g = #{static_friction_g}"
      end
    end
  end

  def make_move
    if @speed_test_phase == 3
      puts "Speed test phase 3: trying maximum acceleration"
      suggested_throttle = 1.0
    elsif @speed_test_phase == 2
      puts "Speed test phase 2: trying maximum deceleration"
      suggested_throttle = 0.0
    end

    piece = @position.piece
    next_piece = piece.next
    next_curve = @track.find_next_curve(@position)

    if @speed_test_phase == 1
      if @static_friction_g_max == 0.0
        # If static friction is not yet determined, drive slow to first curve
        suggested_throttle = 0.5
        if piece.curve?
          puts " - In curve, full speed ahead"
          suggested_throttle = 1.0
        end
      else
        # Determining static friction
        suggested_throttle = 1.0
        if @angle != 0.0
          puts "Speed test phase 1 ended - detected static friction"
          @speed_test_phase = 0
        end
      end
    end

    if @speed_test_phase == 0
      # Normal operation
      suggested_throttle = 1.0

      if piece.curve?
        if @turbo_enabled
          suggested_throttle = 1.0 / @turbo_factor
        end

        maximum_static_friction_lose_velocity = maximum_velocity_at_curve(piece, @position)

        if @angle.abs > @maximum_slip_angle * 0.80 && @velocity > 0 && @velocity > maximum_static_friction_lose_velocity
          puts " - Full brake, because angle too high"
          suggested_throttle = 0.0
        end
      end
    end

    distance_to_next_curve = 9999999
    lane = nil
    if !next_curve.nil?
      distance_to_next_curve = @track.distance_to_piece(@position, next_curve)
      maximum_static_friction_lose_velocity = maximum_velocity_at_curve(next_curve, @position) * @magic_curve_velocity_coefficient

      if maximum_static_friction_lose_velocity > 0
        minimum_braking_distance = braking_distance(@velocity, maximum_static_friction_lose_velocity, 0)
        if @turbo_enabled
          minimum_braking_distance = minimum_braking_distance * 1.5
        end

        if minimum_braking_distance > distance_to_next_curve
          puts " - Full brake... Distance to next curve: #{distance_to_next_curve}, maximum velocity: #{maximum_static_friction_lose_velocity}, minimum braking distance: #{minimum_braking_distance}"
          suggested_throttle = 0.0
        end
      end
    end

    lane = make_lane_changes(@position)
    throttle = suggested_throttle

    if !@am_i_crashed && @turbo_available && !@turbo_enabled && (!@opponent_in_the_way || @track.own_car.last_lap?) && distance_to_next_curve > @longest_straight_length * 0.8 && !piece.curve?
      puts " - Triggering turbo on long straight (turboav: #{@turbo_available} distcur: #{distance_to_next_curve} longstr: #{@longest_straight_length} curve: #{piece.curve?})"
      @tcp.puts turbo_message("Space-paska turbo!!!!111")
      @turbo_available = false
    elsif !lane.nil? && lane.length > 0 && @change_lane_sent_at != piece && @speed_test_phase == 0
      puts " - Changing lane to #{lane}"
      @tcp.puts switch_lane_message(lane)
    else # Always send throttle if no other command
      if @last_throttle != throttle then
        puts " - Changing throttle to #{throttle}"
      end
      @tcp.puts throttle_message(throttle)
      @last_throttle = throttle
    end
  end

  def join_message
    make_msg("join", {:name => @bot_name, :key => @bot_key})
  end

  def create_message(track_name, car_count, password)
    make_msg("createRace", {:botId => {:name => @bot_name, :key => @bot_key}, :trackName => track_name, :carCount => car_count, :password => password})
  end

  def join_race_message_data(data)
    make_msg("joinRace", data)
  end

  def join_race_message(track_name, car_count, password)
    make_msg("joinRace", {:botId => {:name => @bot_name, :key => @bot_key}, :trackName => track_name, :carCount => car_count, :password => password})
  end


  def throttle_message(throttle)
    make_msg("throttle", throttle)
  end

  def switch_lane_message(lane)
    make_msg("switchLane", lane)
  end

  def turbo_message(lane)
    make_msg("turbo", lane)
  end

  def ping_message
    make_msg("ping", {})
  end

  def make_msg(msgType, data)
    JSON.generate({:msgType => msgType, :data => data})
  end

  # Maximum acceleration is the first velocity increase from 0 velocity.

  def estimate_acceleration_coefficient
    -@minimum_acceleration_velocity / @minimum_acceleration
  end

  def estimate_maximum_acceleration_force(velocity)
    acceleration = @maximum_acceleration * (@turbo_enabled ? @turbo_factor : 1.0)
    acceleration - (velocity - acceleration) / estimate_acceleration_coefficient
  end

  def estimate_maximum_deceleration_force(velocity)
    (velocity - @minimum_acceleration_velocity) / -estimate_acceleration_coefficient + @minimum_acceleration
  end

  def estimate_acceleration(velocity, throttle)
    estimate_maximum_acceleration_force(velocity) * throttle + estimate_maximum_deceleration_force(velocity) * (1.0 - throttle)
  end

  def maximum_velocity_at_curve(curve, position)
    actual_radius = curve.radius + (curve.to_the_right? ? -@position.start_lane.distance_from_center : @position.start_lane.distance_from_center)
    Math.sqrt(@static_friction_g_max * actual_radius)
  end

  def braking_distance(from_velocity, to_velocity, throttle)
    # warning, this may never return if throttle is too high!
    distance = 0.0
    velocity = from_velocity
    while velocity > to_velocity
      velocity += estimate_acceleration(velocity, throttle)
      distance += velocity
    end
    distance
  end

  def make_lane_changes(position)
    @lane_change_decided_at_piece ||= nil
    next_piece = position.piece.next
    return if next_piece.nil? || !next_piece.switch?

    # this should make the lane change approx 3 ticks before the switch piece starts
    if @lane_change_decided_at_piece != position.piece && position.in_piece_distance_left < @track.own_car.velocity * 3.0
      @lane_change_decided_at_piece = position.piece
      puts "Next piece is switch => calculating possible lane changes"
      lane_changer = LaneChanger.new(@track)
      change = lane_changer.calculate_optimal_route(@position)
      if change
        puts " - Queue lane change to #{change}"
        return change
      else 
        puts " - Lane change not needed"
        return nil
      end
    end
  end

end