require 'piece'
require 'track'

describe Piece do

  describe "#parse_from_json" do
    it "constructs straight Piece" do
      json = {"length"=>100.0}
      piece = Piece::parse_from_json(json)
      expect(piece).not_to be_nil
      expect(piece.length).to eq(100.0)
      expect(piece.straight?).to be_true
    end
  end

  describe "#straight?" do
    it "returns true for straight" do
    end
  end

  describe "#length" do
    context "when straight" do
      it "returns correct length" do
        length = 42.0
        piece = Piece::create_straight(length)
        expect(piece.length).to eq(length)
      end
    end

    it "returns right length for right curve" do
      radius = 30.0
      angle = 90
      piece = Piece::create_curve(radius, angle)
      expect(piece.length(0.0)).to eq(47.12388980384689)
      expect(piece.length(-15.0)).to eq(70.68583470577035) # left lane is longer
      expect(piece.length(15.0)).to eq(23.561944901923447) # left lane is longer
    end
  end

  describe "comparison operations" do 
    it "works with when equal" do
      p1 = Piece.create_curve(10.0, 45)
      p2 = p1.clone
      expect(p1 == p2).to be_true
      expect(p1 <= p2).to be_true
      expect(p1 >= p2).to be_true
      expect(p1 < p2).to be_false
      expect(p1 > p2).to be_false
    end

    it "works when less than" do
      track = Track.new
      p1 = Piece.create_curve(10.0, 45)
      track.add_piece(p1)
      p2 = p1.clone
      track.add_piece(p2)

      expect(p1 == p2).to be_false
      expect(p1 <= p2).to be_true
      expect(p1 >= p2).to be_false
      expect(p1 < p2).to be_true
      expect(p1 > p2).to be_false
    end
  end

  describe "#lane_radius" do
    context "when straight" do
      piece = Piece::create_straight(42.0)      
    end
    context "when curve to the right" do
      radius = 30.0
      angle = 15.0
      piece = Piece::create_curve(radius, angle)
    end
  end

  describe "#to_s" do
  end

end