require 'track'
require 'lane'

def create_track
  Track.new
end

def create_lane
  Lane.new(-20.0, 0)
end

describe Track do

  describe '#find_lane_by_index' do

    it "returns correct lane" do
      track  = create_track
      lane1  = create_lane
      lane2  = create_lane

      track.add_lane(lane1)
      track.add_lane(lane2)
      
      expect(track.find_lane_by_index(0)).to eq(lane1)
      expect(track.find_lane_by_index(1)).to eq(lane2)
    end
  end

end
