require 'json'
require 'socket'

Dir[File.dirname(__FILE__) + '/lib/*.rb'].each {|file| require file }

server_host = ARGV[0]
server_port = ARGV[1]
bot_name = ARGV[2]
bot_key = ARGV[3]

puts "I'm #{bot_name} and will connect to #{server_host}:#{server_port}"

NorsuBot.new(server_host, server_port, bot_name, bot_key).join