require 'json'
require 'socket'

Dir[File.dirname(__FILE__) + '/lib/*.rb'].each {|file| require file }

server_host = ARGV[0]
server_port = ARGV[1]
bot_name = ARGV[2]
bot_key = ARGV[3]
track_name = ARGV[4]
car_count = ARGV[5]
password = ARGV[6]

puts "I'm #{bot_name} (static speed) and will connect to #{server_host}:#{server_port}"
puts "Will join a game with #{car_count} car(s), track #{track_name} and password #{password}..."

StaticSpeedBot.new(server_host, server_port, bot_name, bot_key).join_race(track_name, car_count, password)
